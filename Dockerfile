# Build remco from specific commit
##################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build rust base
######################
FROM ubuntu:oracular AS base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
USER root

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV RUST_HOME=/home/rust
ENV RUST_UID=10000
ENV RUST_GID=10000

RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    unzip \
    curl \
    wget \
    vim \
    gnupg2 \
    lib32gcc-s1 \
    python3 \
    python3-pip

RUN groupadd -g $RUST_GID rust && \
    useradd -l -s /bin/bash -d $RUST_HOME -m -u $RUST_UID -g rust rust && \
    passwd -d rust

RUN mkdir steamcmd && \
    curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - -C ./steamcmd && \
    cp -vrf steamcmd/* ${RUST_HOME}/ && \
    chmod +x ${RUST_HOME}/steamcmd.sh

COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=root:root remco /etc/remco
RUN chmod -R 0775 /etc/remco

USER rust
RUN pip3 install --user --no-cache-dir --break-system-packages \
  envtpl

USER root
COPY --chown=rust:rust envtpl ${RUST_HOME}/
COPY --chown=rust:rust files/entrypoint.sh ${RUST_HOME}/
RUN chown -Rv $RUST_UID:$RUST_GID /home/rust && \
    chmod +x ${RUST_HOME}/entrypoint.sh


# Build rust image
######################
FROM base as rust
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
USER root

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV RUST_HOME=/home/rust
ENV RUST_UID=10000
ENV RUST_GID=10000
ENV PATH="/home/rust/.local:/home/rust/.local/lib/python3.12/site-packages:$PATH"

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=$CI_COMMIT_AUTHOR
LABEL description="Rust dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/rust-server"
LABEL org.label-schema.description="Rust dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.docker.cmd="docker run -it -d -rm -v /mnt/rust/world1/:/home/rust/server/ -p 28015:28015 -p 28083:28083 registry.gitlab.com/japtain_cack/rust-server"
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

WORKDIR $RUST_HOME
VOLUME "${RUST_HOME}/server"
EXPOSE 28016/tcp
EXPOSE 28015/udp
EXPOSE 28083/tcp

USER rust
ENTRYPOINT ["./entrypoint.sh"]

