[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/rust-server?style=for-the-badge)](https://gitlab.com/japtain_cack/rust-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/rust-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/rust-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/rust-server?style=for-the-badge)](https://gitlab.com/japtain_cack/rust-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)

# rust-server
Run a Rust dedicated server in a Docker container.

This uses steamCMD to automatically update your server software.

This Dockerfile will download the Rust dedicated server app and set it up, along with its dependencies.

If you run the container as is, the `game` directory will be created inside the container, which is inadvisable.
It is highly recommended that you store your game files outside the container using a mount (see the example below).
Ensure that your file system permissions are correct, `chown 1000:1000 mount/path`, and/or modify the UID/GID variables as needed (see below).

It is also likely that you will want to customize your `start.sh` file.
To do this, use the `-e <ENVIRONMENT_VARIABLE>=<value>` for each setting in the `start.sh` file.
The `start.sh` file will be overwritten every time the container is launched. See below for details.


# Run the server
## Docker
Use this `docker run` command to launch a container with a few customized `start.sh` options.
Replace `<ENVIRONMENT_VARIABLE>=<VALUE>` with the appropriate values (see section "Server properties and environment variables" below).

```
docker run --name rust -it --rm \
  -p 28015:28015 \
  -v /home/$(whoami)/rust:/home/rust/server \
  -e RUST_VALIDATE="validate" # can be "" or "validate" \
  -e RUST_SERVER_IP=0.0.0.0 \
  -e RUST_SERVER_PORT=28015 \
  -e RUST_SERVER_HOSTNAME="rust.example.com" \
  -e RUST_CFG_SERVER_DESCRIPTION="Rust server in docker" \
  -e RUST_CFG_GATHER_RATE_1="dispenser * 10"
  -e RUST_CFG_GATHER_RATE_2="pickup * 10"
  -e RUST_CFG_GATHER_RATE_3="quarry * 10"
  -e RUST_CFG_GATHER_RATE_4="survey * 10"
  -e RUST_CFG_DISPENSER_SCALE_1="tree 10"
  -e RUST_CFG_DISPENSER_SCALE_2="ore 10"
  -e RUST_CFG_DISPENSER_SCALE_3="corpse 10"
  -e RUST_USER_SOME_USER='ownerid STEAM_UID_FROM_LOGS "Some User" "note"' \
  registry.gitlab.com/japtain_cack/rust-server
```

### Set selinux context for mounted volumes

`chcon -Rt svirt_sandbox_file_t /path/to/volume`

### Additional Docker commands

**kill and remove all docker containers**

`docker kill $(docker ps -qa); docker rm $(docker ps -qa)`

**docker logs**

`docker logs -f rust`

**attach to the rust server console**

Use `ctrl+p` then `ctrl+q` to quit.

`docker attach rust`

**exec into the container's bash console**

`docker exec rust bash`

**NOTE**: referencing containers by name is only possible if you specify the `--name` flag in your docker run command.

## Helm
* `git clone https://gitlab.com/japtain_cack/rust-server.git`
* `cp rust-server/helm_chart/values.yaml rust_config.yaml`
* Customize `rust_config.yaml` as necessary.
* Install: `helm install -n rust rust-world1 rust-server/helm_chart -f rust_config.yaml`
* Upgrade: `helm upgrade -n rust rust-world1 rust-server/helm_chart -f rust_config.yaml`
* **Keep your `rust_config.yaml` in git**.

# Configuration
## Reference files
* [start.sh](https://gitlab.com/japtain_cack/rust-server/-/blob/master/remco/templates/start.sh)
* [Server Commands](https://developer.valvesoftware.com/wiki/Rust_Dedicated_Server#Server_Commands)
* [Dynamic templates](https://gitlab.com/japtain_cack/rust-server/-/tree/master/envtpl)

## Faster startup, no steamCMD validation
SteamCMD validation is disabled by default, it was causing the server to take a lot more time on boot.
Rust already takes FOREVER to boot, so this is disabled by default.

To enable SteamCMD file validation, in case you need to re-validate your pod/container data. This can
be enabled on demand by adding an environment variable `STEAMCMD_VALIDATE` to your helm values or docker run
command.
 
This project uses [Remco config management](https://github.com/HeavyHorst/remco) and 
[envtpl](https://github.com/envtpl/envtpl).
This allows for templatization of config files and options can be set using environment variables.
This allows for easier deployments using most docker orchistration/management platforms including Kubernetes.

## Environment Variables
### Set user and/or group id (optional)
* `RUST_UID=10000`
* `RUST_GID=10000`

### Statically defined environment variables
Let's start by looking at `start.sh`. You should see something like this `+server.ip "{{ getv("/rust/server/ip", "0.0.0.0") }}"`.
`start.sh` uses statically defined keys, meaning I typed them out fully. Let's break this down.
* `+server.ip` is simply static text, in this example we are setting the `+server.ip` option.
* `{{ ... }}`. Everything in the brackets will be rendered using the templating language processor. 
* `getv("/key", "default_value")` is a function that takes two arguments. This looks up the environment variable based on the
  key, `"/rust/server/ip"`, and sets the value. If there is no value set in the env, the default is used.
* `"/rust/server/ip"` this is a key, which maps to an environment variable. Envs are all CAPS and separated by underscores.
  `"/rust/server/ip"` becomes `RUST_SERVER_IP`. Simple.

## Dynamically generated configs
Now lets consider the [Server Commands](https://developer.valvesoftware.com/wiki/Rust_Dedicated_Server#Server_Commands).
The [server.cfg.tpl](https://gitlab.com/japtain_cack/rust-server/-/blob/master/envtpl/server.cfg.tpl) and
[users.cfg](https://gitlab.com/japtain_cack/rust-server/-/blob/master/envtpl/users.cfg.tpl)
templates handle this via envtpl, due to the shortcomings of the go templating language #Jinja2IsBetter.

You can add any setting from the [Server Commands](https://developer.valvesoftware.com/wiki/Rust_Dedicated_Server#Server_Commands)
very easily. We will be using `server.description` option for this example. All configurations not found in `start.sh` can be set using this method.
* All config environment variables start with the `RUST_CFG_` prefix.
* All environment variables sould be CAPS and separated by underscores.
* All periods, `.`, should be converted to underscores.
* `server.description` becomes `RUST_CFG_SERVER_DESCRIPTION`.

You can also define multiple values for the same property. You can see an example of this in the docker run command above.
* All duplicate properties should follow all rules above
* All duplicate properties should be appended with `_<n>`, where `n` is a number.
* See the below code blocks. This shows the environment variables and the generated file content.

`environment variables`
```
RUST_CFG_GATHER_RATE_1="dispenser * 10"
RUST_CFG_GATHER_RATE_2="pickup * 10"
RUST_CFG_GATHER_RATE_3="quarry * 10"
RUST_CFG_GATHER_RATE_4="survey * 10"
```

`server.cfg`
```
gather.rate dispenser * 10
gather.rate pickup * 10
gather.rate quarry * 10
gather.rate survey * 10
```

# Plugins
Plugins can be installed by providing a url to the file. Plugins are downloaded and installed upon every server boot.
Keep this in mind, when supplying a url. Use a url with a specific version/build to version pin, or use latest if needed.
* Set the `RUST_PLUGIN_URLS` environment variable to either a space or newline deliminated list.

```
RUST_PLUGIN_URLS="""
https://umod.org/plugins/GatherManager.cs
https://umod.org/plugins/foo
https://umod.org/plugins/bar
"""
```

or

```
RUST_PLUGIN_URLS="https://umod.org/plugins/GatherManager.cs https://umod.org/plugins/foo https://umod.org/plugins/bar"
```
