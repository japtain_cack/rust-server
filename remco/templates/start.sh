./RustDedicated -batchmode -nographics \
  +server.ip "{{ getv("/rust/server/ip", "0.0.0.0") }}" \
  +server.port "{{ getv("/rust/server/port", "28015") }}" \
  +rcon.ip "{{ getv("/rust/rcon/ip", "0.0.0.0") }}" \
  +rcon.port "{{ getv("/rust/rcon/port", "28016") }}" \
  +rcon.web "{{ getv("/rust/rcon/web", "0") }}" \
  +server.tickrate "{{ getv("/rust/server/tickrate", "10") }}" \
  +server.hostname "{{ getv("/rust/server/hostname", "rust") }}" \
  +server.identity "{{ getv("/rust/server/identity", "world1") }}" \
  +server.maxplayers "{{ getv("/rust/server/maxplayers", "50") }}" \
  +server.worldsize "{{ getv("/rust/server/worldsize", "3000") }}" \
  +server.seed "{{ getv("/rust/server/seed", "50000") }}" \
  +server.saveinterval "{{ getv("/rust/server/saveinterval", "600") }}" \
  +rcon.password "{{ getv("/rust/rcon/password", "Password123!") }}" \
  #-logfile "{{ getv("/rust/logfile", "/home/rust/server/rust.log") }}"
  #-silent-crashes "{{ getv("/rust/silent-crashes", "-") }}"

