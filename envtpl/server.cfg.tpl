{%- for key,value in environment('RUST_CFG_') %}
{%- set key_list = key.lower().split('_') %}
{%- if key_list[-1].isnumeric() %}
{%- set _ = key_list.pop(-1) %}
{%- endif %}
{{ key_list|join('.') }} {{ value }}
{%- endfor %}
