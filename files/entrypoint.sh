#!/usr/bin/env bash

export REMCO_HOME=/etc/remco
export REMCO_RESOURCE_DIR=${REMCO_HOME}/resources.d
export REMCO_TEMPLATE_DIR=${REMCO_HOME}/templates
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/steamcmd/rust/RustDedicated_Data/Plugins/x86_64
export LD_LIBRARY_PATH=./linux64:./linux32:/home/rust/server/linux64:/home/rust/server/linux32:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
export SteamAppId=258550

date

if [ ! -z $RUST_VALIDATE ]; then
  if [ $RUST_VALIDATE != "validate" ]; then
    echo '[ERROR] RUST_VALIDATE must be one of empty|null|"validate"'
    exit 1
  fi
fi

chown -Rv rust:rust ${RUST_HOME}

cat <<EOF> ${RUST_HOME}/rust.conf
@ShutdownOnFailedCommand 1
@NoPromptForPassword 1
force_install_dir ${RUST_HOME}/server/
login anonymous
app_update 258550 ${STEAMCMD_VALIDATE}
quit
EOF

./steamcmd.sh +runscript ${RUST_HOME}/rust.conf

remco

mkdir -p ${RUST_HOME}/server/server/${RUST_SERVER_IDENTITY:-"world1"}/cfg/
envtpl -o ${RUST_HOME}/server/server/${RUST_SERVER_IDENTITY:-"world1"}/cfg/server.cfg ${RUST_HOME}/envtpl/server.cfg.tpl \
  && echo envtpl: server.cfg in sync
envtpl -o ${RUST_HOME}/server/server/${RUST_SERVER_IDENTITY:-"world1"}/cfg/users.cfg ${RUST_HOME}/envtpl/users.cfg.tpl \
  && echo envtpl: users.cfg in sync

echo
echo "#####################################"
echo installing Oxide
echo
wget -O ${RUST_HOME}/oxide.zip https://github.com/OxideMod/Oxide.Rust/releases/latest/download/Oxide.Rust-linux.zip \
  && unzip -o ${RUST_HOME}/oxide.zip -d ${RUST_HOME}/server/ \
  && rm -rf ${RUST_HOME}/oxide.zip

mkdir -p ${RUST_HOME}/server/oxide/plugins/
rm -rfv ${RUST_HOME}/server/oxide/plugins/*
for url in $(echo "${RUST_PLUGIN_URLS}"); do
  wget -O ${RUST_HOME}/server/oxide/plugins/${url##*/} ${url}
done

echo
echo "#####################################"
echo starting server...
echo
cd ${RUST_HOME}/server/
./start.sh

